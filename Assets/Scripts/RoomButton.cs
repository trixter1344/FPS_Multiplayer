using TMPro;
using UnityEngine;
using Photon.Realtime;

public class RoomButton : MonoBehaviour
{
    public TMP_Text ButtonText;

    private RoomInfo _info;

    public void SetButtonDetails(RoomInfo inputInfo)
    {
        _info = inputInfo;

        ButtonText.text = _info.Name;
    }

    public void OpenRoom()
    {
        Launcher.Instance.JoinRoom(_info);
    }
}
