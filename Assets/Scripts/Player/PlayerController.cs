using UnityEngine;
using Cursor = UnityEngine.Cursor;

public class PlayerController : MonoBehaviour
{
     public Transform ViewPoint;
     public float MouseSensetivity = 1f;
     private float _verticalRotationStore;
     private Vector2 _mouseInput;

     public bool InvertLook;

     public float MoveSpeed = 5f;
     public float RunSpeed = 8f;
     public float JumpForce = 7.0f;
     public float GravityMod = 2.5f;
     private float ActiveMoveSpeed;
     private Vector3 _moveDirection;
     private Vector3 _movement;

     public CharacterController CharacterController;

     private Camera _camera;
     
     
     private bool _isGrounded;
     public Transform _groundCheckPoint;
     public LayerMask groundLayers;

     public GameObject bulletImpact;
     private float _shotCounter;
     public float MuzzleDisplayTime;
     private float _muzzleCounter;

     public float MaxHeat = 10f;
     public float CoolRate = 4f;
     public float OverheatCoolRate = 5f;
     private float _heatCounter;
     private bool _isOverheated;

     public Gun[] AllGuns;
     private int _selectedGun;
     
     // Start is called before the first frame update
     void Start()
     {
         Cursor.lockState = CursorLockMode.Locked;
         
         _camera = Camera.main;
         
         UiController.Instance.WeaponTempSlider.maxValue = MaxHeat;
         
         SwitchGun();

         var newTransform = SpawnManager.Instance.GetSpawmPoint();
         transform.position = newTransform.position;
         transform.rotation = newTransform.rotation;
     }

     // Update is called once per frame
     void Update()
     {
         CameraMovement();

         PlayerMovement();

         ShootingConditions();

         if (Input.GetAxisRaw("Mouse ScrollWheel") > 0)
         {
             _selectedGun++;

             if (_selectedGun >= AllGuns.Length)
             {
                 _selectedGun = 0;
             }

             SwitchGun();
         }
         else if (Input.GetAxisRaw("Mouse ScrollWheel") < 0)
         {
             _selectedGun--;

             if (_selectedGun < 0)
             {
                 _selectedGun = AllGuns.Length - 1;
             }

             SwitchGun();
         }

         for (int i = 0; i < AllGuns.Length; i++)
         {
             if (Input.GetKeyDown($"{i + 1}"))
             {
                 _selectedGun = i;
                 SwitchGun();
             }
         }

         MouseFreeing();
     }
     
     
     void LateUpdate()
     {
         _camera.transform.position = ViewPoint.position;
         _camera.transform.rotation = ViewPoint.rotation;
     }

     private void Shoot()
     {
         var ray = _camera.ViewportPointToRay(new Vector3(.5f, .5f, 0f));
         ray.origin = _camera.transform.position;

         if (Physics.Raycast(ray, out RaycastHit hit))
         {
             Debug.Log($"We hit {hit.collider.gameObject.name}");

             var bulletImpactObject =  Instantiate(bulletImpact, hit.point + (hit.normal * .002f), Quaternion.LookRotation(hit.normal, Vector3.up));
             Destroy(bulletImpactObject, 10f);
         }

         _shotCounter = AllGuns[_selectedGun].TimeBetweenShots;

         _heatCounter += AllGuns[_selectedGun].HeatPerShoot;
         if (_heatCounter >= MaxHeat)
         {
             _heatCounter = MaxHeat;

             _isOverheated = true;
             
             UiController.Instance.OverheatedMessage.gameObject.SetActive(true);
         }
         
         AllGuns[_selectedGun].MuzzleFlash.SetActive(true);
         _muzzleCounter = MuzzleDisplayTime;
     }

     private void ShootingConditions()
     {
         if (AllGuns[_selectedGun].MuzzleFlash.activeInHierarchy)
         {
             _muzzleCounter -= Time.deltaTime;

             if (_muzzleCounter <= 0)
             {
                 AllGuns[_selectedGun].MuzzleFlash.SetActive(false); }
         }

         if (!_isOverheated)
         {
             if (Input.GetMouseButtonDown(0))
             {
                 Shoot();
             }

             if (Input.GetMouseButton(0) && AllGuns[_selectedGun].IsAutomatic)
             {
                 _shotCounter -= Time.deltaTime;

                 if (_shotCounter <= 0)
                 {
                     Shoot();
                 }
             }

             _heatCounter -= CoolRate * Time.deltaTime;
         }
         else
         {
             _heatCounter -= OverheatCoolRate * Time.deltaTime;
             if (_heatCounter <= 0)
             {
                 _isOverheated = false;
                 
                 UiController.Instance.OverheatedMessage.gameObject.SetActive(false);
             }
         }

         if (_heatCounter < 0)
         {
             _heatCounter = 0;
         }

         UiController.Instance.WeaponTempSlider.value = _heatCounter;
     }

     void SwitchGun()
     {
         foreach (var gun in AllGuns)
         {
             gun.gameObject.SetActive(false);
         }
         AllGuns[_selectedGun].gameObject.SetActive(true);
         AllGuns[_selectedGun].MuzzleFlash.SetActive(false);
     }

     void CameraMovement()
    {
        _mouseInput = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y")) * MouseSensetivity;

        transform.rotation = Quaternion.Euler(
            transform.rotation.eulerAngles.x, 
            transform.rotation.eulerAngles.y + _mouseInput.x, 
            transform.rotation.eulerAngles.z);

        _verticalRotationStore += _mouseInput.y;
        _verticalRotationStore = Mathf.Clamp(_verticalRotationStore, -60f, 60f);

        if (InvertLook)
            ViewPoint.rotation = Quaternion.Euler(_verticalRotationStore,
                ViewPoint.rotation.eulerAngles.y,
                ViewPoint.rotation.eulerAngles.z);
        else
            ViewPoint.rotation = Quaternion.Euler(-_verticalRotationStore,
                ViewPoint.rotation.eulerAngles.y,
                ViewPoint.rotation.eulerAngles.z);
    }   
     
     
    void PlayerMovement()
    {
        _moveDirection = new Vector3(Input.GetAxisRaw("Horizontal"), 0f, Input.GetAxisRaw("Vertical"));

        ActiveMoveSpeed = Input.GetKey(KeyCode.LeftShift) ? RunSpeed : MoveSpeed;

        var yVelocity = _movement.y;

        _movement = (transform.forward * _moveDirection.z + transform.right * _moveDirection.x).normalized *
                    ActiveMoveSpeed;


        _movement.y += yVelocity;

        _movement.y += Physics.gravity.y * Time.deltaTime * GravityMod;

        if (CharacterController.isGrounded)
        {
            _movement.y = 0f;
        }

        _isGrounded = Physics.Raycast(_groundCheckPoint.position, Vector3.down, .25f, groundLayers);

        if (Input.GetButtonDown("Jump") && _isGrounded)
        {
            _movement.y = JumpForce;
        }

        CharacterController.Move(_movement * Time.deltaTime);
    }

    void MouseFreeing()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Cursor.lockState = CursorLockMode.None;
        }
        else if (Cursor.lockState == CursorLockMode.None)
        {
            if (Input.GetMouseButtonDown(0))
            {
                Cursor.lockState = CursorLockMode.Locked;
            }
        }
    }

}
   