using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public static SpawnManager Instance;

    void Awake()
    {
        Instance = this;
    }
    
    public Transform[] SpawnPoints;

    void Start()
    {
        foreach (var spawn in SpawnPoints)
        {
            spawn.gameObject.SetActive(false);
        }
    }

    public Transform GetSpawmPoint()
    {
        return SpawnPoints[Random.Range(0, SpawnPoints.Length)];
    }
}
