using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class UiController : MonoBehaviour
{
    public static UiController Instance;

    private void Awake()
    {
        Instance = this;
    }
    
    public TMP_Text OverheatedMessage;
    public Slider WeaponTempSlider;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
