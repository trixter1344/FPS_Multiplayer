using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using TMPro;

public class Launcher : MonoBehaviourPunCallbacks
{
    public static Launcher Instance;

    void Awake()
    {
        Instance = this;
    }

    public GameObject LoadingScreen;
    public TMP_Text LoadingText;
    
    public GameObject MenuButtons;

    public GameObject CreateRoomScreen;
    public TMP_InputField RoomNameInput; 
    
    public GameObject RoomScreen;
    public TMP_Text RoomNameText;    
    public TMP_Text PlayerNameLabel;    
    private List<TMP_Text> _allPlayerNames = new();    
    
    public GameObject ErrorScreen;
    public TMP_Text ErrorText;

    public GameObject RoomBrowserScreen;
    public RoomButton RoomButton;
    private List<RoomButton> _allRoomButtons = new ();
    void Start()
    {
        CloseMenus();
        
        
        LoadingScreen.SetActive(true);
        LoadingText.text = "Connecting to network...";


        PhotonNetwork.ConnectUsingSettings();
    }

    void CloseMenus()
    {
        LoadingScreen.SetActive(false);
        MenuButtons.SetActive(false);
        CreateRoomScreen.SetActive(false);
        RoomScreen.SetActive(false);
        ErrorScreen.SetActive(false);
        RoomBrowserScreen.SetActive(false);
    }

    public override void OnConnectedToMaster() 
    {
        PhotonNetwork.JoinLobby();

        LoadingText.text = "Joining lobby";
    }

    public override void OnJoinedLobby()
    {
        CloseMenus();
        MenuButtons.SetActive(true);

        PhotonNetwork.NickName = Random.Range(0, 1000).ToString();
    }

    public void OpenRoomCreate()
    {
        CloseMenus();
        CreateRoomScreen.SetActive(true);
    }

    public void CreateRoom()
    {
        if (!string.IsNullOrWhiteSpace(RoomNameInput.text))
        {
            var roomOptions = new RoomOptions();

            roomOptions.MaxPlayers = 8;

            PhotonNetwork.CreateRoom(RoomNameInput.text, roomOptions);
            
            CloseMenus();
            LoadingText.text = "Creating Room...";
            LoadingScreen.SetActive(true);
        }
    }

    public override void OnJoinedRoom()
    {
     CloseMenus();
     RoomScreen.SetActive(true);

     RoomNameText.text = PhotonNetwork.CurrentRoom.Name;
     
     ListAllPlayers();
    }

    private void ListAllPlayers()
    {
        foreach (var playerName in _allPlayerNames)
        {
            Destroy(playerName.gameObject);
        }

        _allPlayerNames.Clear();

        var players = PhotonNetwork.PlayerList;


        foreach (var player in players)
        {
            var newPlayerLabel = Instantiate(PlayerNameLabel, PlayerNameLabel.transform.parent);
            newPlayerLabel.text = player.NickName;
            newPlayerLabel.gameObject.SetActive(true);
            
            _allPlayerNames.Add(newPlayerLabel);
        }
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        var newPlayerLabel = Instantiate(PlayerNameLabel, PlayerNameLabel.transform.parent);
        newPlayerLabel.text = newPlayer.NickName;
        newPlayerLabel.gameObject.SetActive(true);
            
        _allPlayerNames.Add(newPlayerLabel);
    }
    
    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        ListAllPlayers();
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        ErrorText.text = $"Failed to create room: {message}";
        CloseMenus();
        ErrorScreen.SetActive(true);
    }

    public void CloseErrorScreen()
    {
        CloseMenus();
        MenuButtons.SetActive(true);
    }

    public void LeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
        LoadingText.text = "Leaving Room...";
        LoadingScreen.SetActive(true);
    }

    public override void OnLeftRoom()
    {
        CloseMenus();
        MenuButtons.SetActive(true);
    }

    public void OpenRoomBrowser()
    {
        CloseMenus();
        RoomBrowserScreen.SetActive(true);
    }
    
    public void CloseRoomBrowser()
    {
        CloseMenus();
        MenuButtons.SetActive(true);
    }

    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        foreach (var rb in _allRoomButtons)
        {
            Destroy(rb.gameObject);
        }
        _allRoomButtons.Clear();
        
        RoomButton.gameObject.SetActive(false);

        foreach (var t in roomList)
        {
            if (t.PlayerCount != t.MaxPlayers && !t.RemovedFromList)
            {
                var newButton = Instantiate(RoomButton, RoomButton.transform.parent);
                
                newButton.SetButtonDetails(t);
                newButton.gameObject.SetActive(true);
                
                _allRoomButtons.Add(newButton);
            }
        }
    }

    public void JoinRoom(RoomInfo inputInfo)
    {
        PhotonNetwork.JoinRoom(inputInfo.Name);
        
        CloseMenus();
        LoadingText.text = "Joining Room...";
        LoadingScreen.SetActive(true);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
