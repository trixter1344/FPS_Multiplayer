using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
  public bool IsAutomatic;
  public float TimeBetweenShots = .1f;
  public float HeatPerShoot = 1f;
  public GameObject MuzzleFlash;
}
